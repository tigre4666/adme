# The best parser json

## System requirements

* php 7.1+
* composer 

## Install

* cd root
* composer install

## Tests

* cd root
* php phpunit.phar ./tests --bootstrap autoload.php

## Example of the use
```
php console json/decode "{\"name\" : \"php\"}"
php console json/decode [1,2,3,4,5]
php console json/decode [[1,0,1],[0,1,0]]
php console json/decode 1000
php console json/decode 1000.50
php console json/decode "\"string\""
```