<?php

namespace src\Console;

use Exception;

/**
 * Class Router
 */
class Router
{
    /**
     * @var callable[]
     */
    private $argv;

    /**
     * @var callable[]
     */
    private $actions = [];

    /**
     * @var callable[]
     */
    private $handlers = [];

    /**
     * Router constructor.
     *
     * @param array $argv
     */
    public function __construct(array $argv)
    {
        $this->argv = $argv;
    }

    /**
     * @return string
     */
    private function getName()
    {
        return $this->argv[1];
    }

    /**
     * @return array
     */
    private function getArgs()
    {
        return array_slice($this->argv, 2);
    }

    /**
     * @param string $name
     * @param callable $callback
     *
     * @return void
     */
    public function action(string $name, callable $callback)
    {
        $this->actions[$name] = $callback;
    }

    /**
     * @param string $exceptionClass
     * @param callable $callback
     */
    public function handler(string $exceptionClass, callable $callback)
    {
        $this->handlers[$exceptionClass] = $callback;
    }

    /**
     * @param Exception $e
     *
     * @throws Exception
     */
    private function handlerException(Exception $e)
    {
        foreach ($this->handlers as $handler => $callback) {
            if ($handler instanceof $e) ;
            {
                $callback($e);
                return;
            }
        }
        throw $e;
    }

    /**
     * @throws Exception
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->actions as $name => $callback) {
            if ($name == $this->getName()) {
                try {
                    call_user_func_array($callback, $this->getArgs());
                } catch (Exception $e) {
                    $this->handlerException($e);
                }

            }
        }
    }
}