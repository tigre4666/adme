<?php

namespace src\JsonParser\exceptions;

use Exception;
use Throwable;

/**
 * Class JsonParserException
 */
class JsonParserException extends Exception
{
    /**
     * @inheritdoc
     */
    public function __construct(string $message = "Not valid json", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}