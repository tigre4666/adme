<?php

namespace src\JsonParser;


use src\JsonParser\factories\JsonParserFactory;

/**
 * Class Json
 */
class Json
{
    /**
     * @param string $source
     *
     * @return array|mixed|integer|string|float
     */
    public static function decode(string $source) {
        return JsonParserFactory::factory()->parser($source);
    }
}