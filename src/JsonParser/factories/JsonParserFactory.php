<?php
/**
 * Created by PhpStorm.
 * User: bond
 * Date: 2/19/2018
 * Time: 12:06 AM
 */

namespace src\JsonParser\factories;


use src\JsonParser\parsers\ArrayParser;
use src\JsonParser\parsers\DictParser;
use src\JsonParser\parsers\FloatParser;
use src\JsonParser\parsers\IntegerParser;
use src\JsonParser\parsers\JsonParser;
use src\JsonParser\parsers\StringParser;

/**
 * Class JsonParserFactory
 */
class JsonParserFactory
{
    /**
     * @return JsonParser
     */
    public static function factory()
    {
        return new JsonParser([
            new ArrayParser(),
            new DictParser(),
            new IntegerParser(),
            new FloatParser(),
            new StringParser()
        ]);
    }
}