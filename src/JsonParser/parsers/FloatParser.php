<?php

namespace src\JsonParser\parsers;

/**
 * Class FloatParser
 */
class FloatParser extends AbstractParser
{
    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^-?[0-9]+\.[0-9]+$#';
    }

    /**
     * @inheritdoc
     */
    protected function getValue(array $matches)
    {
        return floatval($matches[0]);
    }
}