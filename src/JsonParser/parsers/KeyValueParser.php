<?php

namespace src\JsonParser\parsers;

/**
 * Class KeyValueParser
 */
class KeyValueParser extends AbstractParser
{
    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^"(.+)"\s{0,5}:\s{0,5}(.+)$#';
    }

    /**
     * @param array $matches
     *
     * @return array [ket,value]
     */
    protected function getValue(array $matches)
    {
        return [$matches[1], $matches[2]];
    }
}