<?php
namespace src\JsonParser\parsers;

/**
 * Class IntegerParser
 */
class IntegerParser extends AbstractParser
{
    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^-?[0-9]+$#';
    }

    /**
     * @inheritdoc
     */
    protected function getValue(array $matches)
    {
        return intval($matches[0]);
    }
}