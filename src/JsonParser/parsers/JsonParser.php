<?php

namespace src\JsonParser\parsers;

use src\JsonParser\exceptions\JsonParserException;
use src\JsonParser\interfaces\Parser;
use src\JsonParser\interfaces\TryParser;

/**
 * Class JsonParser
 */
class JsonParser implements Parser
{
    /**
     * @var TryParser[]
     */
    private $parsers;

    public function __construct(array $parsers)
    {
        $this->parsers = $parsers;
    }

    /**
     * @inheritdoc
     */
    function parser(string $source)
    {
        $source = trim($source);
        foreach ($this->parsers as $parser) {
            if ($parser->tryParser($source, $value)) {
                if (is_array($value)) {
                    $result = [];
                    foreach ($value as $key => $json) {
                        $result[$key] = $this->parser($json);
                    }
                    return $result;
                }
                return $value;
            }
        }
        throw new JsonParserException('Not valid source : ' . $source);
    }
}