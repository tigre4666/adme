<?php
namespace src\JsonParser\parsers;

/**
 * Class StringParser
 */
class StringParser extends AbstractParser
{
    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^"(.+)?"$#';
    }

    /**
     * @inheritdoc
     */
    protected function getValue(array $matches)
    {
        return $matches[1];
    }
}