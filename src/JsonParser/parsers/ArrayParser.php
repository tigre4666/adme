<?php
namespace src\JsonParser\parsers;

/**
 * Class ArrayParser
 */
class ArrayParser extends AbstractParser
{
    /**
     * @var SplitParser
     */
    private $split;

    public function __construct()
    {
        $this->split = new SplitParser();
    }

    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^\[(.+)?\]$#';
    }

    /**
     * @inheritdoc
     */
    protected function getValue(array $matches)
    {
        if($matches[1]==null) {
            return [];
        }
        return $this->split->parser($matches[1]);
    }
}