<?php

namespace src\JsonParser\parsers;
/**
 * Class DictParser
 */
class DictParser extends AbstractParser
{
    /**
     * @inheritdoc
     */
    protected function getRegular(): string
    {
        return '#^{(.+)?}$#';
    }

    /**
    * @inheritdoc
     */
    protected function getValue(array $matches)
    {
        if($matches[1] ==null) {
            return [];
        }
        $split = new SplitParser();
        $keyValueParser = new KeyValueParser();
        $result = [];
        foreach ($split->parser($matches[1]) as $keyValueString) {
            list($key, $value) = $keyValueParser->parser($keyValueString);
            $result[$key] = $value;
        }
        return $result;
    }
}