<?php

namespace src\JsonParser\parsers;

use SplStack;
use src\JsonParser\exceptions\JsonParserException;
use src\JsonParser\interfaces\Parser;

/**
 * Class SplitParser
 */
class SplitParser implements Parser
{
    const MAP = [
        '{' => '}',
        '[' => ']'
    ];
    /**
     * @var SplStack
     */
    private $closed;
    /**
     * @var Boolean
     */
    private $inSting;

    /**
     * @var string[]
     */
    private $buffer;

    /**
     * @var array
     */
    private $result;

    /**
     * @inheritdoc
     */
    public function parser(string $source)
    {
        $this->init();
        foreach (str_split($source) as $char) {
            $this->next($char);
        }
        if ($this->tryPush()) {
            return $this->result;
        }
        throw new  JsonParserException();
    }

    /**
     * @return  void
     */
    private function init()
    {
        $this->closed = new SplStack();
        $this->inSting = false;
        $this->buffer = [];
        $this->result = [];
    }

    /**
     * @return bool
     */
    private function tryPush(): bool
    {
        if ($this->buffer && !$this->inSting && $this->closed->isEmpty()) {
            $this->result[] = implode('', $this->buffer);
            $this->buffer = [];
            return true;
        }
        return false;
    }

    /**
     * @param string $chart
     *
     * @throws JsonParserException
     */
    private function next(string $chart)
    {
        if ($chart == '"') {
            $this->inSting = !$this->inSting;
        }
        if (!$this->inSting) {
            if (isset(self::MAP[$chart])) {
                $this->closed->push(self::MAP[$chart]);
            }
            if (in_array($chart, self::MAP)) {
                if ($chart != $this->closed->pop()) {
                    throw new  JsonParserException();
                }
            }
        }
        if ($chart == ',' && $this->tryPush()) {
            return;
        }
        $this->buffer[] = $chart;
    }
}