<?php

namespace src\JsonParser\parsers;


use src\JsonParser\exceptions\JsonParserException;
use src\JsonParser\interfaces\Parser;
use src\JsonParser\interfaces\TryParser;

/**
 * Class AbstractParser
 */
abstract class  AbstractParser implements TryParser, Parser
{
    /**
     * @inheritdoc
     */
    public function tryParser(string $source, &$value): bool
    {
        if (preg_match($this->getRegular(), $source, $matches)) {
            $value = $this->getValue($matches);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function parser(string $source)
    {
        if ($this->tryParser($source, $value)) {
            return $value;
        }
        throw new JsonParserException('Not valid source : ' . $source);
    }

    /**
     * @inheritdoc
     */
    protected abstract function getRegular(): string;

    /**
     * @inheritdoc
     */
    protected abstract function getValue(array $matches);


}