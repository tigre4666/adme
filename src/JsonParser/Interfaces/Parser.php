<?php

namespace src\JsonParser\interfaces;

/**
 * Interface Parser
 */
interface Parser
{
    /**
     * @param string $source
     *
     * @return mixed
     */
    function parser(string $source);
}