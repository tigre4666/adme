<?php
namespace src\JsonParser\interfaces;

/**
 * Interface TryParser
 */
interface TryParser
{
    /**
     * @param string $source
     * @param mixed $value
     *
     * @return bool
     */
    function tryParser(string $source, &$value): bool;
}