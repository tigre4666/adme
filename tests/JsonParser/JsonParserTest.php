<?php

namespace tests\JsonParser;

use PHPUnit\Framework\TestCase;
use src\JsonParser\Json;

class JsonParserTest extends TestCase
{
    public function testInts()
    {
        $this->assertEquals(1, Json::decode('1'));
        $this->assertEquals(10000, Json::decode('10000'));
        $this->assertEquals(123, Json::decode(' 123 '));
        $this->assertEquals(-100, Json::decode(' -100 '));
    }

    public function testFloats()
    {
        $this->assertEquals(1.0, Json::decode('1.0'));
        $this->assertEquals(10.44, Json::decode('10.44'));
        $this->assertEquals(-100.123, Json::decode(' -100.123 '));
    }

    public function testString()
    {
        $this->assertEquals('', Json::decode('""'));
        $this->assertEquals('php developer', Json::decode('"php developer"'));
        $this->assertEquals("python data science", Json::decode('"python data science"'));

    }

    public function testArrays()
    {
        $this->assertEquals([], Json::decode('[]'));
        $this->assertEquals([1, 2, 3], Json::decode('[1,2,3]'));
        $this->assertEquals([1.3, 2.1, 3.2], Json::decode('[1.3,2.1,3.2]'));
        $this->assertEquals(["php", "python"], Json::decode('["php","python"]'));
        $this->assertEquals([[1, 2, 3], [3, 2, 1]], Json::decode('[[1,2,3],[3,2,1]]'));
        $this->assertEquals(['Php, js'], Json::decode('["Php, js"]'));
    }

    public function testDictionary()
    {
        $this->assertEquals([], Json::decode('{}'));
        $this->assertEquals(['python' => 1, 'php' => 2], Json::decode('{"python":1,"php":2}'));
        $this->assertEquals(['python' => [1, 2, 3], 'php' => [3, 2, 1]], Json::decode('{"python":[1,2,3],"php":[3,2,1]}'));
        $this->assertEquals(['key' => 'value'], Json::decode('{"key" : "value"}'));
    }

    /**
     * @expectedException \src\JsonParser\exceptions\JsonParserException
     *
     * @dataProvider  exceptionProvider
     *
     * @param string $source
     */
    public function testException(string $source)
    {
        Json::decode($source);
    }

    public function exceptionProvider()
    {
        return [
            ['I am Json'],
            ['{key : "invalid"}'],
            ['[1,[{],2]']
        ];
    }
}