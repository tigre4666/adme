<?php
$loader = require __DIR__ . '/vendor/autoload.php';
$loader->addPsr4('src\\', __DIR__.'./src');
$loader->addPsr4('tests\\', __DIR__.'./tests');
return $loader;
?>